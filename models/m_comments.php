<?php
require_once ("database.php");
class m_comments extends database
{
    public function read_comments()
    {
        $sql = "select * from comments";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }


    public function read_comments_by_idroom($id)
    {
        $sql = "select * from comments where id_room  = ?";
        $this->setQuery($sql);
        return $this->loadRowid(array($id));
    }

    public function  add_comments($id_cmt,$id_room,$date,$email,$content,$status)
    {
        $sql ="insert into comments values(?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id_cmt,$id_room,$date,$email,$content,$status));
    }
    

    
}

?>