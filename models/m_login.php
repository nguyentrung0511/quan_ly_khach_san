<?php
include_once ("database.php");
class m_login extends database{
    public function read_user_by_id_password($full_name,$password){
        $sql="SELECT * FROM `customer` WHERE full_name=? and password = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($full_name,($password)));
    }

    public function read_user_by_username($username){
        $sql="SELECT * FROM `customer` WHERE full_name=?";
        $this->setQuery($sql);
        return $this->loadRow(array($username));
    }
}
