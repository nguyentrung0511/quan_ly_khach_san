<?php
require_once ("database.php");
class m_reservation_step extends database {
    public function  read_room_category(){
        $sql="SELECT * FROM `room_category`";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }


    function add_customer($full_name,$address,$email,$phone_number,$status) {
        $sql = "INSERT INTO customer(full_name,address,email,phone_number,status) VALUES(?,?,?,?,?)";
        $this->setQuery($sql);
        $result = $this->execute(array($full_name,$address,$email,$phone_number,$status));
        if($result)
            return $this->getLastId();  //If query execute successful, the system will return lastID in table khach_hang
        else

            return false;
    }
    public function read_service()
    {
        $sql = "select * from service";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_id_service($id)
    {
        $sql = "select * from service where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }

    function add_bill($id_customer, $id_room,$id_service, $arrive, $departure,$total_bill,$description,$status) {
        $sql = "INSERT INTO bill(id_customer, id_room, id_service, arrive, departure,total_bill,description,status) VALUES(?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        $result = $this->execute(array($id_customer, $id_room,$id_service, $arrive, $departure,$total_bill,$description,$status));
        if($result)
            return $this->getLastId();
        else
            return false;
    }

    function read_bill(){
        $sql ="SELECT * FROM bill";
        $this->setQuery($sql);
        return $this->loadAllRows();

    }


//    function read_bill_by_id($id){
//        $sql = "SELECT customer.full_name,customer.email,customer.phone_number, room.room_name, room.picture, room.price, bill.arrive,bill.departure,bill.total_bill FROM customer, room, bill WHERE bill.id_customer = customer.id AND bill.id_room=room.id AND bill.id = ?;";
//        $this->setQuery($sql);
//        return $this->loadRows(array($id));
//    }
    function read_bill_by_id($id_room){
        $sql = "SELECT * FROM  bill as b,room as  WHERE bill.id_room = room.id AND  bill.id_room = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id_room));
    }

    function read_customer_by_id($id_customer){
        $sql = "select * From customer where id =?";
        $this->setQuery($sql);
        return $this->loadRow(array($id_customer));
    }
}


