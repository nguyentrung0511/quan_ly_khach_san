

<!-- SUB BANNER -->
<section class="section-sub-banner bg-9">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2>VIEW</h2>
                <p>Luxury Hotel</p>
            </div>
        </div>
    </div>
</section>
<!-- END / SUB BANNER -->

<div class="courses-page-area3">
    <div class="container">
        <div class="row" style="margin-top: 10px ; margin-bottom: 20px">
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <img style="width: 848px;height: 423px;" src="admin/public/assets/images/picture_room/<?php echo $room->picture;?>" class="img-responsive" alt="course" width="150px" height="200px">
                        <div class="course-details-inner">
                            <h2 class="title-default-left title-bar-high"><?php echo $room->room_name?></h2>
                            <p><?php echo $room->description?></p>
                            <h3 class="sidebar-title">Thông tin về phòng</h3>
                            <ul class="course-feature">
                                <li>Kích thước: 35 m2 / 376 ft2 </li>
                                <li>Số người: 2-3 </li>
                                <li>Dịch vu: Ăn uống, spa</li>
                                <li>Giá: <?php echo number_format($room->price);?> VND/Day </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="bot">
                    <b>Quý khách hãy kiểm tra tình trạng phòng để đặt phòng nhé!</b>
                    <a  class="awe-btn awe-btn-13" href="reservation_step1.php?id=<?php echo $room->id; ?>&id_room_category=<?php echo $room->id_room_category;?>">CHECK</a>
                </div>


                <section class="cmt">
                    <b>Mời bạn để lại ý kiến bình luận bên dưới:</b>
                    <form method="post">
                        <section class="cmt_area">
                            <h6>Email:</h6>
                            <input type="text" name="email" style="width: 100%;">
                            <h6>Bình luận:</h6>
                            <textarea name="content" rows="5" style="width: 100%;"></textarea>
                        </section>
                        <div class="cmt_submit">
                            <input class="awe-btn awe-btn-13" type="submit" name="submit" value="Xác nhận">
                        </div>
                    </form>
                    <h5>Bình luận gần đây:</h5>
                    <?php for($i=0;$i<count($comments);$i+=1){?>
                        
                        <div class="box_cmt">
                            
                            <span class="cmt_email"><?php echo $comments[$i]->email; ?></span> <span><?php echo $comments[$i]->date; ?></span>
                            <p><?php echo $comments[$i]->content;?></p>
                        </div>
                    <?php }; ?>
                </section>
                
            </div>

       
        </div>
    </div>
</div>

<script>

    $(document).ready(function() {
        console.log(123);
    })


    $(".register-room-order").click(function() {
        const isLogged = $("#is-logged").val();
        if(isLogged == "none") {
            $("#login-button").addClass("open");
            $("#login-form").css("display", "block");
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#login-form").offset().top
            }, 500);
        }
        else {
            var href = $(this).data("href");
            window.location.href = href;
        }
    });
</script>

<style>
    .cmt_area h6{
        padding: 10px 0;
    }
    .cmt h5{
        margin-bottom: 10px;
        text-decoration: underline;
    }
    .cmt .box_cmt p{
        background-color: #cccccc;
        padding: 7px;
        border-radius: 5px;
        font-size: 13px;
    }
    .cmt .box_cmt .cmt_email{
        font-weight: bolder;
        margin-bottom: 5px;
        margin-top: 5px;
        font-size: 15px;
    }
    .cmt_submit{
        padding-bottom: 20px;
        padding-top: 10px;
    }
</style>