<!-- SUB BANNER -->
<section class="section-sub-banner awe-parallax bg-16">

    <div class="awe-overlay"></div>

    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2>Booking</h2>
                <p>Luxury Hotel</p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->

<!-- RESERVATION -->
<section class="section-reservation-page bg-white">

    <div class="container">
        <div class="reservation-page">
            <!-- STEP -->
            <?php include("v_reservation_step.php") ?>
            <!-- END / STEP -->
            <form method="POST" action="">
                <div class="row">

                    <!-- SIDEBAR -->
                    <div class="col-md-4 col-lg-3">

                        <div class="reservation-sidebar">

                            <!-- RESERVATION DATE -->
                            <div class="reservation-date bg-gray">

                                <!-- HEADING -->
                                <h2 class="reservation-heading">Dates</h2>
                                <!-- END / HEADING -->

                                <ul>
                                    <div class="check_availability-field">
                                        <label>Arrive</label>
                                        <input type="text" name="arrive" class="awe-calendar awe-input from" value="<?php echo  (isset($_GET['arrive'])) ? $_GET['arrive'] : ''  ?>" placeholder="Arrive">

                                    </div>

                                    <div class="check_availability-field">
                                        <label>Depature</label>
                                        <input type="text" name="departure" class="awe-calendar awe-input to" value="<?php echo  (isset($_GET['departure'])) ? $_GET['departure'] : ''  ?>" placeholder="Departure">
                                    </div>

                                </ul>

                            </div>
                            <!-- END / RESERVATION DATE -->

                            <!-- ROOM SELECT -->
                            <div class="reservation-room-selected bg-gray">

                                <!-- HEADING -->
                                <h2 class="reservation-heading">Room</h2>
                                <!-- END / HEADING -->

                                <!-- ITEM -->
                                <div class="reservation-room-seleted_item">
                                    <div class="courses-img-wrapper hvr-bounce-to-bottom">
                                        <img class="img-responsive" style="width: 250px;height: 190px;" src="admin/public/assets/images/picture_room/<?php echo $rooms->picture;?>" alt="room">
                                    </div>
                                    <div class="reservation-room-seleted_name has-package">
                                        <h2><a href="room_detail.php"><?php echo $rooms->room_name?></a></h2>
                                    </div>
                                    <div class="reservation-room-seleted_package">
                                        <h6>Price</h6>
                                        <ul>
                                            <?php echo number_format($rooms->price)?> VND/Day
                                        </ul>
                                    </div>



                                </div>
                                <!-- END / ITEM -->


                            </div>
                            <!-- END / ROOM SELECT -->

                        </div>

                    </div>
                    <!-- END / SIDEBAR -->

                    <!-- CONTENT -->
                    <div class="col-md-8 col-lg-9">

                        <div class="reservation_content">

                            <div class="reservation-billing-detail">

    <!--                            <p class="reservation-login">Returning customer? <a href="login.php">Click here to login</a></p>-->

                                <h4>Booking Room</h4>

                                <div class="row">

                                    <div class="col-sm-6">
                                        <label>FULL NAME<sup>*</sup></label>
                                        <input name="full_name" type="text" class="input-text" required>
                                    </div>
                                </div>


                                <label>Address<sup>*</sup></label>
                                <input name="address" type="text" class="input-text" placeholder="Street Address" required>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Email Address<sup>*</sup></label>
                                        <input name="email" type="text" class="input-text" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Phone<sup>*</sup></label>
                                        <input name="phone_number" type="text" class="input-text" required>
                                    </div>
                                </div>

                                <label>Dịch vụ<sup>*</sup></label>
                                <select class="input-group" style="width: 500px; height: 35px" id="id_dich_vụ" name="id_dich_vụ"  >
                                    <?php foreach ($service as $item) {
                                        ?>
                                        <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label>Ghi chú</label>
                                <textarea name="description" class="input-textarea" placeholder="Notes about your order, eg. special notes for delivery"></textarea>


<!--                                <ul class="option-bank" name="payment">-->
<!--                                    <li>-->
<!--                                        <label class="label-radio">-->
<!--                                            <input type="radio" class="input-radio" name="chose-bank">-->
<!--                                            Direct Bank Transfer-->
<!--                                        </label>-->
<!--                                        <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>-->
<!--                                    </li>-->
<!---->
<!--                                    <li>-->
<!--                                        <label class="label-radio">-->
<!--                                            <input type="radio" class="input-radio" name="chose-bank">-->
<!--                                            Cheque Payment-->
<!--                                        </label>-->
<!--                                    </li>-->
<!---->
<!--                                    <li>-->
<!--                                        <label class="label-radio">-->
<!--                                            <input type="radio" class="input-radio" name="chose-bank">-->
<!--                                            Credit Card-->
<!--                                        </label>-->
<!---->
<!--                                        <img src="public/layout/images/icon-card.jpg" alt="">-->
<!--                                    </li>-->
<!---->
<!--                                </ul>-->

                            </div>

                        </div>
                        <div style="margin-left: 300px">
                            <button class="awe-btn awe-btn-13" name="btnBook">Booking</button>
                        </div>
                    </div>
                    <!-- END / CONTENT -->

                </div>
            </form>
        </div>
    </div>



</section>
<!-- END / RESERVATION -->

<script>

    $(document).ready(function() {
        console.log(123);
    })


    $(".register-room-order").click(function() {
        const isLogged = $("#is-logged").val();
        if(isLogged == "none") {
            $("#login-button").addClass("open");
            $("#login-form").css("display", "block");
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#login-form").offset().top
            }, 500);
        }
        else {
            var href = $(this).data("href");
            window.location.href = href;
        }
    });
</script>

