
<!-- SUB BANNER -->
<section class="section-sub-banner bg-9">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2>ROOMS</h2>
                <p>luxury Hotel</p>
            </div>
        </div>
    </div>
</section>

<section class="section-blog bg-white" style="width: 100%; background-color: #fff8e9">
    <div class="container"  style="height: 500px; margin-left: 180px; ">
        <div class="blog">
                <div class="row" style="margin-top: 10px ; margin-bottom: 20px">
                <div class="col-md-8 col-md-offset-2">
                    <div class="blog-content" style="margin-top: 50px" >

                        <!-- POST SINGLE -->
                        <img style="margin-top: 50px" src="public/layout/images/bg.png">
                        <h2 style="margin-left: 65px" >Chúc mừng bạn đã đặt phòng thành công!</h2>
                        <p style="margin-left: 150px">Thông tin đặt phòng của bạn đã được gửi về mail. Vui lòng xác nhận.</p>
                        <a style="margin-left: 300px" > Quay lại <a style="color: #fbb040" class='thongbao' href='index.php'>Trang chủ</a>

                            <!-- END / POST SINGLE -->

                        <!-- COMMENT -->

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- END / BLOG -->

<script>

    $(document).ready(function() {
        console.log(123);
    })


    $(".register-room-order").click(function() {
        const isLogged = $("#is-logged").val();
        if(isLogged == "none") {
            $("#login-button").addClass("open");
            $("#login-form").css("display", "block");
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#login-form").offset().top
            }, 500);
        }
        else {
            var href = $(this).data("href");
            window.location.href = href;
        }
    });
</script>

