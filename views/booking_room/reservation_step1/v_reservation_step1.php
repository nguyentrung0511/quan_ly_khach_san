<!-- SUB BANNER -->
<section class="section-sub-banner bg-16">

    <div class="awe-overlay"></div>

    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2>Choose Room</h2>
                <p>Luxury Hotel</p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->

<!-- RESERVATION -->
<!-- Courses Page 1 Area Start Here -->
<div class="courses-page-area1">
    <div class="container">
        <div class="row">
            <!-- RESERVATION -->
            <section class="section-reservation-page bg-white">

                <div class="container">
                    <div class="reservation-page">


                        <div class="row">

                            <div class="col-md-4 col-lg-3">

                                <div class="reservation-sidebar">

                                    <!-- SIDEBAR AVAILBBILITY -->
                                    <div class="reservation-sidebar_availability bg-gray">

                                        <!-- HEADING -->
                                        <h2 class="reservation-heading">Rooms</h2>
                                        <!-- END / HEADING -->
                                        <form method="GET" action="" id="checkout-form">
                                            <div class="check_availability-field">
                                                <label>Arrive</label>
                                                <input type="text" name="arrive" class="awe-calendar awe-input from" value="<?php echo  (isset($_GET['arrive'])) ? $_GET['arrive'] : ''  ?>" placeholder="Arrive">
                                            </div>

                                            <div class="check_availability-field">
                                                <label>Departure</label>
                                                <input type="text" name="departure" class="awe-calendar awe-input to" value="<?php echo  (isset($_GET['departure'])) ? $_GET['departure'] : ''  ?>" placeholder="Departure">
                                            </div>
                                            <div>
                                                <button style="height: 30px; width: 100px; margin-top: 20px;margin-left: 5px; background-color:#E1BD85FF;color:#fff;border:2px "  type="submit" name="btnCheck" value="">CHECK</button>
                                                <button style="height: 30px; width: 100px; margin-top: 20px;margin-left: 15px; background-color:#E1BD85FF;color:#fff;border:2px "  type="reset"  value="">RESET</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- END / SIDEBAR AVAILBBILITY -->

                                </div>

                            </div>

                            <div class="col-md-8 col-lg-9">
                                <br class="reservation_content bg-gray">
                                <!-- STEP -->
                                <?php include("v_reservation_step.php") ?>
                                <!-- END / STEP -->


                                <div class="row">
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="margin-top: 20px">
                                        <div role="tabpanel" class="tab-pane active" id="gried-view">
                                            <?php
                                            if(!empty($newRoom)){
                                            foreach ($newRoom as  $value) {
                                                    ?>

                                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="height: 250px" >
                                                        <div class="courses-box1">
                                                            <div class="single-item-wrapper" style="margin-left: 10px">
                                                                <div class="courses-img-wrapper hvr-bounce-to-bottom">
                                                                    <img class="img-responsive" style="    width: 250px;height: 150px;" src="admin/public/assets/images/picture_room/<?php echo $value['picture'];?>" alt="room">
                                                                </div>
                                                                <div class="courses-content-wrapper">
                                                                    <h3  class="reservation-room_name"><a><?php echo $value['room_name'];?></a></h3>
                                                                    <h class="item-title">
                                                                        <b>Price:</b>
                                                                        <?php echo number_format($value['price']); ?> VND/Day
                                                                    </h>
                                                                    <div style="margin-left: 13px">
                                                                        <a  class="awe-btn awe-btn-13" href="reservation_step2.php?id=<?php echo $value['id']; ?>&id_room_category=<?php echo $value['id_room_category'];?>&arrive=<?php echo $arrive;?>&departure=<?php echo $departure;?>">BOOK</a>
                                                                        <a  class="awe-btn awe-btn-13" href="room_detail.php?id=<?php echo $value['id']; ?>&id_room_category=<?php echo $value['id_room_category'];?>">VIEW</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                            <?php }
                                            }else{
                                                echo "<div class='alert alert-info' style='margin-left: 40px; margin-right: 30px' >Hãy cho chúng tôi biết ngày đến và ngày đi của Quý khách!</div>";
//                                                echo "Hãy cho chúng tôi biết ngày đến và ngày đi của Quý khách! ";
                                            }

                                            ?>

                                            <div class="clearfix"></div>

                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <ul class="pagination-left">

                                        </ul>
                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
        </div>

        </section>
        <!-- END / RESERVATION -->

    </div>
</div>
</div>

<!-- Courses Page 1 Area End Here -->


