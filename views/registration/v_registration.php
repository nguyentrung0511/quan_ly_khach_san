<!-- Registration Page Area Start Here -->
<section class="section-account parallax bg-11">
    <div class="awe-overlay"></div>
    <div class="container">
        <div class="login-register">

            <h2 class="sidebar-title">Đăng ký tài khoản</h2>
            <form form id="checkout-form" method="post"  >
                <?php
                include_once 'views/error.php';
                $errorClass = new CError();
                $errorClass->showError($error_1);
                $errorClass->showError($success);
                //Error::showError($error_1);
                ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
<!--                            <label class="control-label" for="email">Địa chỉ email *</label>-->
                            <input type="email" id="email" name="email" class="form-control" placeholder="Email *" required>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
<!--                            <label class="control-label" for="first-name">Họ và tên *</label>-->
                            <input type="text" id="user-name" name="full_name" class="form-control" placeholder="Họ và tên *" required>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
<!--                            <label class="control-label" for="phone">Mật khẩu *</label>-->
                            <input type="password" id="password" name="password"  class="form-control" placeholder="Mật khẩu *" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
<!--                            <label class="control-label" for="phone">Xác nhận mật khẩu *</label>-->
                            <input type="password" id="phone" name="confirm_password" class="form-control" placeholder="Xác nhận mật khẩu *" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
<!--                            <label class="control-label" for="phone">Số điện thoại liên hệ *</label>-->
                            <input type="phone" id="phone" name="phone_number" class="form-control" placeholder="Số điện thoại liên hệ *" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="pLace-order mt-30">
                            <button class="view-all-accent-btn disabled" type="submit" name="btnSave" value="Login">Đăng kí</button>
                        </div>
                    </div>
                </div>
                <span class="account-desc">I have an account  -  <a href="login.php">LOGIN</a></span>
            </form>

        </div>
    </div>
</section>
<!-- Registration Page Area End Here -->
