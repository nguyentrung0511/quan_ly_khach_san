<!-- HOME GUEST BOOK -->
<div class="section-home-guestbook awe-parallax bg-13">
    <div class="container">
        <div class="home-guestbook">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="guestbook-content owl-single">
                        <!-- ITEM -->
                        <div class="guestbook-item">
                            <div class="img">
                                <img src="public/layout/images/avatar/img-5.jpg" alt="">
                            </div>

                            <div class="text">
                                <p>This is the only place to stay in Catalina! I have stayed in the cheaper hotels and they were fine, but this is just the icing on the cake! After spending the day bike riding and hiking to come back and enjoy a glass of wine while looking out your ocean view window and then to top it all off...</p>
                                <span><strong>Seelentag</strong></span><br>
                                <span>From Los Angeles, California</span>
                            </div>
                        </div>
                        <!-- ITEM -->

                        <!-- ITEM -->
                        <div class="guestbook-item">
                            <div class="img">
                                <img src="public/layout/images/avatar/img-5.jpg" alt="">
                            </div>

                            <div class="text">
                                <p>This is the only place to stay in Catalina! I have stayed in the cheaper hotels and they were fine, but this is just the icing on the cake! After spending the day bike riding and hiking to come back and enjoy a glass of wine while looking out your ocean view window and then to top it all off...</p>
                                <span><strong>Seelentag</strong></span><br>
                                <span>From Los Angeles, California</span>
                            </div>
                        </div>
                        <!-- ITEM -->

                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<!-- END / HOME GUEST BOOK -->