<?php
@session_start();
if (isset($_SESSION['staff_admin'])) {
include('controllers/c_room.php');
$c_room = new c_room();
$c_room->show_room();
} else {
    header("location:login.php");
}
?>