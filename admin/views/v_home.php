<div id="main-wrapper">
    <div  style="border: 1px solid #ccc;
    margin:48px 27px 57px 0px;
    background: #fff; margin-left: 30px">
        <div class="page-wrapper" >
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" >
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row" style="margin-left: 90px;margin-top: 90px">
                    <div class="col-md-3" >
                        <div class="card card-hover" style="width: 250px; height: 70px">
                            <div class="box bg-warning text-center" >
                                <h1 class="font-light text-white"><i class="mdi mdi-collage"></i></h1>
                                <a href="room_category.php" class="box-font-light" style="color: #0b0b0b"><b>Quản Lý Loại Phòng</b></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card card-hover" style="width: 250px; height: 70px; margin-left: 50px">
                            <div class="box bg-cyan text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-collage"></i></h1>
                                <a href="room.php" class="box-font-light" style="color: #0b0b0b"><b>Quản Lý Phòng</b></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card card-hover" style="width: 250px; height: 70px; margin-left: 100px" >
                            <div class="box bg-success text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-collage"></i></h1>
                                <a href="bill.php" class="box-font-light" style="color: #0b0b0b"><b>Quản Lý Hóa Đơn</b></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left: 200px; margin-top: 50px">
                    <div class="col-md-3" >
                        <div class="card card-hover" style="width: 250px; height: 70px; margin-left: 50px">
                            <div class="box bg-purple text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-collage"></i></h1>
                                <a href="customer.php" class="box-font-light" style="color: #0b0b0b"><b>Quản Lý Khách Hàng</b></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" >
                        <div class="card card-hover" style="width: 250px; height: 70px; margin-left: 120px">
                            <div class="box bg-orange text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-collage"></i></h1>
                                <a href="staff.php" class="box-font-light" style="color: #0b0b0b"><b>Quản Lý Nhân Viên</b></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div style="width:100%;color:#4f3fff; padding-bottom:15px;">
                            <canvas id="buyers" ></canvas>
                        </div>
                        <script>

                            // get line chart canvas
                            var buyers = document.getElementById('buyers').getContext('2d');

                            var myLineChart = new Chart(buyers,{
                                type: 'line',
                                data: <?php echo $strJSON;?>,
                                options: {
                                    responsive: true,
                                    title: {
                                        //display: true,
                                        //position:'right',
                                        //text:'Doanh thu năm 2016'
                                    },// end title
                                    scales: {
                                        xAxes: [{
                                            display: true,
                                            scaleLabel: {
                                                display: true,
                                                labelString: 'Tháng Năm',

                                            }
                                        }],
                                        yAxes: [{
                                            display: true,
                                            scaleLabel: {
                                                display: true,
                                                labelString: 'Trị giá'
                                            },
                                            ticks: {
                                                suggestedMin: -10,
                                                suggestedMax: 250,
                                            }
                                        }]
                                    }
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
