
<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 57%;">
        <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='staff.php'">Quay lại </button>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Chi tiết thông tin Nhân Viên</h4>
                                <fieldset>
                                    <style>
                                        table {
                                            font-family: arial, sans-serif;
                                            border-collapse: collapse;
                                            width: 100%;
                                        }

                                        td, th {
                                            border: 1px solid #dddddd;
                                            text-align: left;
                                            padding: 8px;
                                        }
                                    </style>
                                    <table>
                                        <tr>
                                            <th>Mã nhân viên:</th>
                                            <th><?php echo $staff->id?></th>
                                        </tr>
                                        <tr>
                                            <th>Tên bộ phận:</th>
                                            <th><?php echo $role->name_role?></th>
                                        </tr>
                                        <tr>
                                            <th>Tên nhân viên:</th>
                                            <th><?php echo $staff->staff_name?></th>
                                        </tr>
                                        <tr>
                                            <th>Tên đăng nhập:</th>
                                            <th><?php echo $staff->username?></th>
                                        </tr>
                                        <tr>
                                            <th>Mật khẩu:</th>
                                            <th><?php echo $staff->password?></th>
                                        </tr>
                                        <tr>
                                            <th>Ngày sinh:</th>
                                            <th><?php echo $staff->birth_day?></th>
                                        </tr>
                                        <tr>
                                            <th>Giới tính:</th>
                                            <th><?php echo $staff->sex?></th>
                                        </tr>
                                        <tr>
                                            <th>Số Cmt:</th>
                                            <th><?php echo $staff->passport_number?></th>
                                        </tr>
                                        <tr>
                                            <th>Số điện thoại:</th>
                                            <th><?php echo $staff->phone_number?></th>
                                        </tr>
                                        <tr>
                                            <th>Địa chỉ:</th>
                                            <th><?php echo $staff->address?></th>
                                        </tr>
                                        <tr>
                                            <th>Vị trí công việc:</th>
                                            <th><?php echo $staff->position?></th>
                                        </tr>
                                        <tr>
                                            <th>Mô tả vị trí công việc:</th>
                                            <th><?php echo $staff->description?></th>
                                        </tr>
                                        <tr>
                                            <th>Trạng thái:</th>
                                            <th style="background-color: <?php echo $staff->status ?>" ><?php echo $staff->status ? "Đang làm việc" : "Đã thôi việc"?></th>
                                        </tr>

                                    </table>
                                </fieldset>
                        </form>
                        <br>
                        <form method="POST" action="testexcel.php" enctype="multipart/form-data" class="text-right <?php echo $hidden?>">
                            <button class="btn btn-danger" type="submit" name="btnGui">Thêm bảng lương cho nhân viên </button>
                            <input type="file" name="file">
                            <input type="hidden" name="id_nhan_vien" value="<?php echo $staff->id;?>" ?>
                        </form>
                        <form method="POST" action="list_payroll.php" enctype="multipart/form-data" class="text-right <?php echo $hidden?>">
                            <button class="btn btn-danger" style="margin-right: 700px; margin-top: -55px" type="submit" name="btnGui">Chi tiết bảng chấm công </button>
                            <input type="hidden" name="id_nhan_vien" value="<?php echo $staff->id;?>" ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
