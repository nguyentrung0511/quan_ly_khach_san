<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 53%">
        <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='home.php'">Quay lại </button>
        <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='add_staff.php'" id="" onclick="">Thêm </button>
    </div>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="content-box-header">
                <h3><?php echo $tieude;?></h3>
                <div class="clear"></div>
            </div>
            <!-- End .content-box-header -->
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th><b>Mã nhân viên</b></th>
                            <th><b>Tên bộ phận</b></th>
                            <th><b>Tên nhân viên</b></th>
                            <th><b>Ngày sinh</b></th>
                            <th><b>Giới tính</b></th>
                            <th><b>Số Cmt</b></th>
                            <th><b>Số điện thoại</b></th>
                            <th><b>Địa chỉ</b></th>
                            <th><b>Trạng thái</b></th>
                            <th><b>Hành động</b></th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="pagination"> <?php if ($count>8)
                                    {echo $lst;}
                                    ?>  </div>
                                <div class="clear"></div></td>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach ($staffs as $staff) {
                            ?>
                            <tr>
                                <td><a title="Edit"><?php echo $staff->id;?></a></td>
                                <td><?php echo $staff->name_role ?></td>
                                <td><a title="Detail"><?php echo $staff->staff_name;?></td>
                                <td><?php echo $staff->birth_day;?></td>
                                <td><?php echo $staff->sex;?></td>
                                <td><?php echo $staff->passport_number;?></td>
                                <td><?php echo $staff->phone_number;?></td>
                                <td><?php echo $staff->address;?></td>
                                <td><span style="margin-inside: 15%;"
                                          class="badge badge-pill badge-<?php if($staff->status==1)
                                          {
                                              echo "info";
                                          }
                                          else
                                          {
                                              echo "danger";
                                          }
                                          ?>
                                        float-right"><?php if($staff->status==1)
                                        {
                                            echo "Đang làm việc";
                                        }
                                        else
                                        {
                                            echo "Đã nghỉ việc";
                                        }?></span>
                                </td>
                                <td><!-- Icons -->
                                    <a href="edit_staff.php?id=<?php echo $staff->id?>" title="Edit">
                                        <img src="public/layout/resources/images/icons/pencil.png" alt="Edit"/>
                                    </a>
                                    <a href="detail_staff.php?id=<?php echo $staff->id;?>" title="Detail">
                                        <img src="public/assets/images/icon/resume.png" width="15px" height="15px" alt="Detail"/>
                                    </a>
                                    <!-- <a href="javascript:hoi_delete_staff(<?php echo $staff->id;?>)" title="Delete">
                                        <img src="public/layout/resources/images/icons/cross.png" alt="Delete"/>
                                    </a> -->
                                   <a href="delete_staff.php?id=<?php echo $staff->id;?>" title="Delete">
                                        <img src="public/layout/resources/images/icons/cross.png" alt="Delete"/>
                                    </a> 
                                   <!-- <a href="javascript:delete_role_staff(<?php echo $role->id;?>)" title="Delete">
                                        <img src="public/layout/resources/images/icons/cross.png" alt="Delete"/>
                                    </a> -->
                                </td> 
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>