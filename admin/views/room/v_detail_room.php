
<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 57%;">
        <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='room.php'">Quay lại </button>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Chi tiết Phòng</h4>
                                <fieldset>
                                    <style>
                                        table {
                                            font-family: arial, sans-serif;
                                            border-collapse: collapse;
                                            width: 100%;
                                        }

                                        td, th {
                                            border: 1px solid #dddddd;
                                            text-align: left;
                                            padding: 8px;
                                        }
                                    </style>
                                    <table>
                                        <tr>
                                        <th>Mã phòng:</th>
                                             <th><?php echo $room->id?></th>
                                        </tr>
                                        <tr>
                                        <th>Tên phòng:</th>
                                            <th><?php echo $room->room_name?></th>
                                        </tr>
                                        <tr>
                                        <th>Loại phòng:</th>
                                            <th><?php echo $category->name?></th>
                                        </tr>
                                        <tr>
                                        <th>Đơn giá:</th>
                                            <th><?php echo $room->price?></th>
                                        </tr>
                                        <tr>
                                        <th>Hình ảnh:</th>
                                            <th><img src="public/assets/images/picture_room/<?php echo $room->picture;?>" width="290px" /></th>
                                        </tr>
                                        <tr>
                                        <th>Mô tả:</th>
                                            <th><?php echo $room->description?></th>
                                        </tr>
                                        <tr>
                                        <th>Trạng thái:</th>
                                            <th style="background-color: <?php echo $room->status ?>"> <?php echo $room->status ? "Trống" : "Hết"?> </th>
                                        </tr>

                                    </table>
                                </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
