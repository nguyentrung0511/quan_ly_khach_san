<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 53%">
        <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='home.php'">Quay lại </button>
        <!-- <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='add_room.php'" id="" onclick="">Thêm </button> -->
    </div>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="content-box-header">
                <h3><?php echo $tieude;?></h3>
                <div class="clear"></div>
            </div>
            <!-- End .content-box-header -->
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th><b>Mã phản hồi</b></th>
                            <th><b>Tên phòng</b></th>
                            <th><b>Ngày phản hồi</b></th>
                            <th><b>Email</b></th>
                            <th><b>Phản hồi</b></th>
                            <th><b>Trạng thái</b></th>
                            <th><b>Hành động</b></th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="pagination div_trang"><?php if ($count>8)
                                    {echo $lst;}
                                    ?> </div>
                                <div class="clear"></div></td>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach ($rooms as $room)
                        {
                            ?>

                            <tr>
                                <td><?php echo $room->id_cmt;?></td>
                                <td><?php echo $room->room_name;?></td>
                                <td><?php echo $room->date;?></td>
                                <td><?php echo $room->email;?></td>
                                <td><?php echo $room->content;?></td>
                                
                                <td><span style="margin-right: 15%;"
                                          class="badge badge-pill badge-<?php if($room->status==1)
                                          {
                                              echo "info";
                                          }
                                          else
                                          {
                                              echo "danger";
                                          }

                                          ?>
                                        float-right"><?php if($room->status==1)
                                        {
                                            echo "Hiển thị";
                                        }
                                        else
                                        {
                                            echo "Không hiển thị";
                                        }?></span>
                                </td>
                                <td><!-- Icons -->
                                    <a href="delete_comments.php?id=<?php echo $room->id_cmt;?>" title="Delete">
                                        <img src="public/layout/resources/images/icons/cross.png" alt="Delete"/>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
</div>
