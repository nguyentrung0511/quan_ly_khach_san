<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 53%">
        <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='home.php'">Quay lại </button>
        <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='add_service.php'" id="" onclick="">Thêm </button>
    </div>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="content-box-header">
                <h3><?php echo $tieude;?></h3>
                <div class="clear"></div>
            </div>
            <!-- End .content-box-header -->
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                    <form method="POST" enctype="multipart/form-data">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th><b>STT</b></th>
                                <th><b>Tên gói dịch vụ</b></th>
                                <th><b>Giá dịch vụ</b></th>
                                <th><b>Các dịch vụ được cung cấp</b></th>
                                <th><b>Trạng thái</b></th>
                                <th><b>Hành động</b></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <td colspan="6">
                                    <div class="pagination div_trang"><?php if ($count>8)
                                        {echo $lst;}
                                        ?> </div>
                                    <div class="clear"></div></td>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php
                            $i= 1;
                            foreach ($service as $item)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $item->name;?></td>
                                    <td><?php echo $item->price;?></td>
                                    <td><?php echo $item->note;?></td>
                                    <td><span style="margin-inside: 15%;"
                                              class="badge badge-pill badge-<?php if($item->status==1)
                                              {
                                                  echo "info";
                                              }
                                              else
                                              {
                                                  echo "danger";
                                              }
                                              ?>
                                        float-right"><?php if($item->status==1)
                                            {
                                                echo "Đang cung cấp";
                                            }
                                            else
                                            {
                                                echo "Đã dừng cung cấp";
                                            }?></span>
                                    </td>
                                    <td>
                                        <a href="edit_service.php?id=<?php echo $item->id?>" title="Edit">
                                            <img src="public/layout/resources/images/icons/pencil.png" alt="Edit"/>
                                        </a>
                                        <a href="delete_service.php?id=<?php echo $item->id;?>" title="Delete">
                                            <img src="public/layout/resources/images/icons/cross.png" alt="Delete"/>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
