
<div id="main-wrapper">
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Sửa thông tin Dịch Vụ</h4>
                                <fieldset>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Tên dịch vụ</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Tên dịch vụ" value="<?php echo $service->name;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Giá dịch vụ</label>
                                        <div class="col-sm-9">
                                            <input type="number" minlength="8" class="form-control" id="price" name="price" placeholder="Giá dịch vụ" value="<?php echo $service->price;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Các dich được cung cấp</label>
                                        <div class="col-sm-9">
                                            <textarea type="text" class="form-control" id="note" name="note">
                                                <?php echo $service->note;?>
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="status">
                                                <option>--Chọn--</option>
                                                <option value="1">Đang cung cấp</option>
                                                <option value="0">Đã thôi cung cấp</option>
                                            </select>
                                        </div>
                                    </div>
                                    <p>
                                        <input class="btn btn-default" type="submit" value="Lưu thay đổi" name="btnCapnhat"/>
                                        <input class="btn btn-default" type="button" value="Bỏ qua" onclick="window.location='service.php'" />
                                    </p>
                                </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
