<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 53%">
        <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='home.php'">Quay lại </button>
    </div>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="content-box-header">
                <h3><?php echo $tieude;?></h3>
                <div class="clear"></div>
            </div>
            <!-- End .content-box-header -->
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th><b>Mã khách hàng</b></th>
                            <th><b>Tên khách hàng</b></th>
                            <th><b>Địa chỉ</b></th>
                            <th><b>Email</b></th>
                            <th><b>Số điện thoại</b></th>
                            <th><b>Trạng thái</b></th>
                            <th><b>Hành động</b></th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="pagination div_trang"><?php if ($count>8)
                                    {echo $lst;}
                                    ?> </div>
                                <div class="clear"></div></td>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach ($customers as $customer)
                        {
                            ?>

                            <tr>
                                <td><?php echo $customer->id;?></td>
                                <td><?php echo $customer->full_name;?></td>
                                <td><?php echo $customer->address;?></td>
                                <td><?php echo $customer->email;?></td>
                                <td><?php echo $customer->phone_number;?></td>
                                <td><span style='margin-left: 5px'
                                          class="badge badge-pill badge-<?php if($customer->status==1)
                                          {
                                              echo "info";
                                          }
                                          else
                                          {
                                              echo "danger";
                                          }

                                          ?>
                                        float-right"><?php if($customer->status==1)
                                        {
                                            echo "<div >Hoạt động </div>";
                                        }
                                        else
                                        {
                                            echo "<div >Không hoạt động</div>";
                                        }?></span>
                                </td>
                                <td><!-- Icons -->
                                    <a style="margin-left: 20px" href="detail_customer.php?id=<?php echo $customer->id;?>" title="Detail">
                                        <img src="public/assets/images/icon/resume.png" width="15px" height="15px" alt="Detail"/>
                                    </a>

                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
</div>
