<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 53%">
        <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='home.php'">Quay lại </button>
    </div>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="content-box-header">
                <h4>Bảng lương nhân viên: <?php echo $show_name->staff_name;?></h4>
                <div class="clear"></div>
            </div>
            <!-- End .content-box-header -->
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th><b>STT</b></th>
                            <th><b>Ngày Làm</b></th>
                            <th><b>Trạng thái</b></th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tbody>
                        <?php
                        $i = 1;
                        foreach ($show_payroll as $item) {
                            ?>
                            <tr>
                                <td><a title="Edit"><?php echo $i++;?></a></td>
                                <td><?php echo $item->work_day ?></td>
                                <td><span style="margin-inside: 15%;"
                                          class="badge badge-pill badge-<?php if($item->status==1)
                                          {
                                              echo "info";
                                          }
                                          else
                                          {
                                              echo "danger";
                                          }
                                          ?>
                                        float-right"><?php if($item->status==1)
                                        {
                                            echo "Có làm việc";
                                        }
                                        else
                                        {
                                            echo "Không làm việc";
                                        }?></span>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>