<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 53%">
        <button  style="margin-top: 20px; margin-bottom: 20px" class="btn btn-default" onclick="window.location.href='home.php'">Quay lại </button>
    </div>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="content-box-header">
                <h3><?php echo $tieude;?></h3>
                <div class="clear"></div>
            </div>
            <!-- End .content-box-header -->
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                    <form method="POST" enctype="multipart/form-data">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th><b>Mã hóa đơn</b></th>
                            <th><b>Tên khách hàng</b></th>
                            <th><b>Tên phòng</b></th>
                            <th><b>Ngày đến</b></th>
                            <th><b>Ngày đi</b></th>
                            <th><b>Tổng hóa đơn</b></th>
                            <th><b>Mô tả</b></th>
                            <th><b>Trạng thái</b></th>
                            <th><b>Hành động</b></th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="pagination div_trang"><?php if ($count>8)
                                    {echo $lst;}
                                    ?> </div>
                                <div class="clear"></div></td>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach ($bills as $bill)
                        {
                            ?>
                            <tr>
                                <td><?php echo $bill->id; ?>
                                    <input type="hidden" value="<?php echo $bill->id ?>" name="id[]" />
                                </td>
                                <td><?php echo $bill->full_name;?></td>
                                <td><?php echo $bill->room_name;?></td>
                                <td><?php echo $bill->arrive;?></td>
                                <td><?php echo $bill->departure;?></td>
                                <td><?php echo number_format($bill->total_bill);?></td>
                                <td><?php echo $bill->description;?></td>
                                <td><select  name="ds[]" style="background-color: <?php echo $bill->status ? "#da542e" : "#2255a4" ?>;color: white;" >
                                        <option value="1" <?php echo($bill->status==1)?"selected":"" ?> >Chưa thanh toán</option>
                                        <option value="0" <?php echo($bill->status==0)?"selected":"" ?>>Đã thanh toán</option>

                                    </select>  </td>
                                <td>
                                    <a style="margin-left: 25px" href="detail_bill.php?id=<?php echo $bill->id;?>" title="Detail">
                                        <img src="public/assets/images/icon/resume.png" width="15px" height="15px" alt="Detail"/>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <p>
                        <input class="btn btn-default" type="submit" value="Cập nhật" name="btnCapnhat" onclick="" />
                        <input class="btn btn-default" type="button" value="Bỏ qua" onclick="window.location='bill.php'" />
                    </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
