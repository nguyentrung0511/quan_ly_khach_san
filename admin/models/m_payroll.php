<?php
include_once("database.php");
class m_payroll extends database
{

    public function add_payroll($id,$work_day,$status,$id_staff)
    {

        $sql ="INSERT INTO payroll VALUES (?,?,?,?)";
        $this->setQuery($sql);
//
        return $this->execute(array(NULL,$work_day,$status,$id_staff));
    }
    public function read_all_id_payroll($id)
    {
        $sql = "SELECT p.* FROM payroll AS p, staff AS s WHERE s.id = p.id_staff AND p.id_staff = ".$id;

        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_all_date_payroll($id,$date)
    {
        $sql = "SELECT p.* FROM payroll AS p, staff AS s WHERE s.id = p.id_staff AND p.id_staff = ".$id." AND p.work_day = '".$date."'";

        $this->setQuery($sql);
        return $this->loadRow();
    }
    public function edit_date_payroll($status,$id,$date)
    {
        $sql="update payroll set status=? where id_staff=? and work_day =?";
        $this->setQuery($sql);
        return $this->execute(array($status,$id,$date));
    }
}