<?php
include_once ("database.php");
class m_comments extends database
{
    public function read_comments($vt = -1, $limit = -1){
        $sql = "SELECT * FROM `comments`";
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }
        $this->setQuery($sql);
        return $this->loadAllRows();

    }

    public function read_comments_for_id_room($id)
    {
        $sql = "SELECT * FROM `room` WHERE id_room = ?";
        $this->setQuery($sql);
        return $this->loadRowid(array($id));
    }
    public function read_cmt_with_room_id($vt = -1, $limit = -1, $id_room)
    {
        $sql='SELECT comments.id_cmt, room.room_name, comments.date, comments.email, comments.content, comments.status FROM comments, room where comments.id_room = ? and room.id = comments.id_room order by id_cmt desc';
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }
        $this->setQuery($sql);
        return $this->loadAllRows(array($id_room));
    }
    public function delete_comments($id)
    {
        $sql = "DELETE FROM `comments` WHERE id_cmt = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}
?>