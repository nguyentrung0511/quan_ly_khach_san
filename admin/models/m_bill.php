<?php
include_once ("database.php");
class m_bill extends database
{
    public function read_bill($vt = -1, $limit = -1){
    $sql = "SELECT * FROM `bill`";
    if ($vt >= 0 && $limit > 0) {
        $sql .= " limit $vt,$limit";
    }
    $this->setQuery($sql);
    return $this->loadAllRows();
    }

    public function read_bill_with_customer_and_room($vt = -1, $limit = -1)
    {
        $sql='SELECT bill.id,customer.full_name,room.room_name,bill.arrive,bill.departure,bill.total_bill,bill.description,bill.status FROM bill,customer,room WHERE bill.id_customer = customer.id and bill.id_room = room.id';
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }

        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_bill_by_id($id)
    {
        $sql = "SELECT * FROM `bill` WHERE id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }

    public function delete_bill($id){

        $sql = "delete from bill where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
    public function update_status($id,$status)
    {
        $sql="update bill set status=? where id=?";
        $this->setQuery($sql);
        return $this->execute(array($status,$id));
    }
}
?>
