<?php
include_once ("database.php");
class m_staff extends database {

    public function read_staff(){
        $sql="SELECT * FROM staff";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_staff_by_id_password($username,$password) {
        $sql = "SELECT * FROM `staff` where username = ? and password = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($username,$password));
    }

    public function read_staff_with_role($vt = -1, $limit = -1)
    {
        $sql='SELECT staff.id,role.name_role,staff.staff_name,staff.username,staff.password,staff.birth_day,staff.sex,staff.passport_number,staff.phone_number,staff.address,staff.position,staff.description,staff.status FROM role,staff WHERE staff.id_role = role.id order by id desc';
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function read_staff_by_id($id)
    {
        $sql = "SELECT * FROM `staff` WHERE id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }

    public function add_staff($id,$id_role,$staff_name,$username,$password,$birth_day,$sex,$passport_number,$phone_number,$address,$position,$description,$status)
    {
        $sql ="insert into staff values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array('NULL',$id_role,$staff_name,$username,$password,$birth_day,$sex,$passport_number,$phone_number,$address,$position,$description,$status));
    }
    public function edit_staff($id,$id_role,$staff_name,$username,$password,$birth_day,$sex,$passport_number,$phone_number,$address,$position,$description,$status)
    {
        $sql="update staff set id_role=?,staff_name=?,username=?,password=?,birth_day=?,sex=?,passport_number=?,phone_number=?,address=?,position=?,description=?,status=? Where id=?";
        $this->setQuery($sql);
        return $this->execute(array($id_role,$staff_name,$username,$password,$birth_day,$sex,$passport_number,$phone_number,$address,$position,$description,$status,$id));
    }
    public function delete_staff($id){

        $sql = "delete from staff where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}