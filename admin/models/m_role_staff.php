<?php
include_once ("database.php");
class m_role_staff extends database
{
    public function read_role_staff($vt = -1, $limit = -1){
        $sql = "SELECT * FROM `role`";
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }
        $this->setQuery($sql);
        return $this->loadAllRows();

    }
    public function read_role_staff_by_id($id)
    {
        $sql = "SELECT * FROM `role` WHERE id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function add_role_staff($id,$name_role)
    {
        $sql ="insert into role values(?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$name_role));
    }
    public function edit_role_staff($id,$name_role)
    {
        $sql="update role set name_role = ?  Where id=?";
        $this->setQuery($sql);
        return $this->execute(array($id,$name_role));
    }


    public function delete_role_staff($id){

        $sql = "delete from role where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}
?>