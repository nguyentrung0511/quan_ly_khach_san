<?php
include_once ("database.php");
class m_service extends database{
    public function read_service($vt = -1, $limit = -1){
        $sql = "SELECT * FROM `service`";
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function add_service($id,$name,$price,$status,$note)
    {
        $sql ="insert into service values(?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$name,$price,$status,$note));
    }
    public function read_service_by_id($id)
    {
        $sql = "SELECT * FROM `service` WHERE id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function edit_service($id,$name,$price,$status,$note)
    {
        $sql="update service set name=?, price=?, starts=?, note=? Where id=?";
        $this->setQuery($sql);
        return $this->execute(array($name,$price,$status,$note,$id));
    }
    public function delete_service($id){

        $sql = "delete from service where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}