
<?php
@session_start();
if (isset($_SESSION['staff_admin'])) {
    include('controllers/c_room_category.php');
    $c_room_category = new c_room_category();
    $c_room_category->show_room_category();
} else {
    header("location:login.php");
}
?>
