<?php
include ("models/m_service.php");
class c_service{
    public function show_service()
    {
        $m_service = new m_service();
        $service = $m_service->read_service();
        $count=count($service);
        // Phân trang
        include("lib/Pager.php");
        $p=new Pager();
        $limit=8;
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $service = $m_service->read_service($vt,$limit);

        $view = 'views/service/v_service.php';
        $title = "Service Management";
        $tieude = "Dịch vụ khách sạn";
        include('templates/layout.php');
    }
    public function add_service()
    {
        if(isset($_POST["btnThem"]))
        {
            $id= null;
            $name = $_POST["name"];
            $price = $_POST["price"];
            $note = $_POST["note"];
            $status = $_POST["status"];
            $m_service = new m_service();
            $kq=$m_service->add_service($id,$name,$price,$status,$note);
            if($kq)
            {
                echo "<script>alert('Thêm thành công');window.location='service.php'</script>";
            }
            else
            {
                echo "<script>alert('Thêm không thành công')</script>";
            }

        }

        // View
        $view = 'views/service/v_add_service.php';
        include('templates/layout.php');

    }
    public function edit_service()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_service = new m_service();
            $service  = $m_service->read_service_by_id($id);
            // Cập nhật
            if(isset($_POST["btnCapnhat"]))
            {

                $name = $_POST["name"];
                $price = $_POST["price"];
                $note = $_POST["note"];
                $status = $_POST["status"];

                $kq=$m_service->edit_service($id,$name,$price,$status,$note);
                if($kq)
                {
                    echo "<script>alert('Cập nhật thành công');window.location='service.php'</script>";
                }
                else
                {
                    echo "<script>alert('Cập nhật không thành công')</script>";
                }
            }
            // End Cập nhật

            // View
            $view = 'views/service/v_edit_service.php';
            include('templates/layout.php');
        }
    }
    public function delete_service()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_service =new m_service();
            $kq = $m_service->delete_service($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='service.php'</script>";
            }

        }


    }
}