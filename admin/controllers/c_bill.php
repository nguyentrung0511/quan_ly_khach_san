<?php
include("models/m_bill.php");
include("models/m_customer.php");
include("models/m_room.php");
include("models/m_room_category.php");
class c_bill
{
    public function show_bill()
    {
        $m_bill = new m_bill();
        if(isset($_POST["btnCapnhat"]) && isset($_POST["ds"]) && isset($_POST["id"]))
        {

            // Cập nhật lại tình trạng
            $ds=$_POST["ds"];
            $bill=$_POST["id"];
            for($i=0;$i<count($bill);$i++)
            {
                $id=$bill[$i];
                $status=$ds[$i];
                $m_bill->update_status($id,$status);
            }
        }
        $m_bill = new m_bill();
        $bills = $m_bill->read_bill_with_customer_and_room();
        $m_room_category = new m_room_category();
        $category = $m_room_category->read_room_category();
        $count=count($bills);
        // Phân trang
        include("lib/Pager.php");
        $p=new Pager();
        $limit=8;
        $count=count($bills);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $bills = $m_bill->read_bill_with_customer_and_room($vt,$limit);

        $view = 'views/bill/v_bill.php';
        $title = "Bill Management";
        $tieude = "Hóa đơn";
        include('templates/layout.php');
    }




    public function delete_bill()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_bill = new m_bill();
            $kq = $m_bill->delete_bill($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='bill.php'</script>";
            }

        }


    }
    public function show_detail_bill()
    {
        $m_bill = new m_bill();
        $bills = $m_bill->read_bill();
        $id = $bills[0]->id;
        $m_customer = new m_customer();
        $customer = $m_customer->read_customer_by_id($id);
        $m_room = new m_room();
        $room = $m_room->read_room_for_id($id);
        if(isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        $bills = $m_bill->read_bill_by_id($id);
        $view = 'views/bill/v_detail_bill.php';
        include ('templates/layout.php');
    }
}
?>
