<?php
include ("models/m_payroll.php");
include ("models/m_staff.php");
class c_payroll {
    public function show_payroll(){
        if(isset($_POST['id_nhan_vien'])){
            $id = $_POST['id_nhan_vien'];
        }

        $m_staff = new m_staff();
        $show_name = $m_staff->read_staff_by_id($id);
        $m_payroll = new m_payroll();
        $show_payroll = $m_payroll->read_all_id_payroll($id);
        $view = 'views/payroll/v_list_payroll.php';
        include('templates/layout.php');
    }

    public function edit_payroll(){
        if (isset($_GET['id']) && isset($_GET['date'])){
            $id = $_GET['id'];
            $date = $_GET['date'];
        }
        $m_staff = new m_staff();
        $show_name = $m_staff->read_staff_by_id($id);

        $m_payroll = new m_payroll();
        $show_payroll_id = $m_payroll->read_all_date_payroll($id,$date);
        if(isset($_POST["btnCapnhat"])){
            $status = $_POST["status"];
            $kq = $m_payroll->edit_date_payroll($status,$id,$date);
            if($kq)
            {
                echo "<script>alert('Cập nhật thành công');window.location='staff.php'</script>";
            }
            else
            {
                echo "<script>alert('Cập nhật không thành công')</script>";
            }
        }
        $view = 'views/payroll/v_edit_payroll.php';
        include('templates/layout.php');
    }
}