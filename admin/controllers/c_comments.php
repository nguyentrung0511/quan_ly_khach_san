<?php

include ("models/m_comments.php");

class c_comments
{
    function show_comments($id_room)
    {
// Models
        // $id_room = $_GET['id'];
        $m_comments = new m_comments();
        $rooms = $m_comments->read_comments_for_id_room($id_room);

        $count=count($rooms);
        // Phân trang
        include("lib/Pager.php");
        $p=new Pager();
        $limit=8;
        $count=count($rooms);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $rooms = $m_comments->read_cmt_with_room_id($vt,$limit,$id_room);
        $view = 'views/comments/v_comments.php';
        $title = "Room Management";
        $tieude = "Đánh giá khách hàng";
        include('templates/layout.php');
// View
    }

   
    function delete_comments()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_room = new m_comments();
            $kq = $m_room->delete_comments($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='comments.php'</script>";
            }

        }

    }

}
?>