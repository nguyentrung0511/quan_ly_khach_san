<?php
@session_start();
class c_room{

    public function show()
    {
        $isPaging = true;
        include ("models/m_room.php");
        include ("models/m_category.php");

        $m_room = new m_room();

        $room = $m_room->read_room();
        $id = $room[0]->id;
        if(isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        $room = $m_room->read_room_by_category($id);
        $count=count($room);
        // Phân trang 1
        include("libs/Pager.php");
        $p=new pager();
        $limit=6;
        $count=count($room);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $room=$m_room->read_room_by_category($id,$vt,$limit);
        $room_full = $m_room->read_room_by_category_full($id_room_category);

        if (isset($_POST["btnTimPhong"]))
        {
            $isPaging = false;
            $room_name = $_POST["room_name"];
            $room= array($m_room->read_room_by_idroom($id));
            $room_full = $m_room->read_room_by_category_full($id_room_category);
            $view = 'views/room/v_room.php';
            include 'templates/frontend/layout.php';
            return;
        }
        $view = 'views/room/v_room.php';
        include 'templates/frontend/layout.php';


    }
    public  function show_detail_room()
    {
        include ("models/m_room.php");
        include ("models/m_comments.php");
        $m_comments = new m_comments();
        $m_room = new m_room();
        $room = $m_room->read_room();
        $comments = $m_comments->read_comments();
        $id = $room[0]->id;
        if($_POST["submit"]){
            $email = $_POST["email"];  
            $pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^";  
            if (!preg_match ($pattern, $email) ){  
                echo "Email không hợp lệ.";
            }elseif(isset($_POST["content"]))
            {
                // $id_cmt = 1;
                $content = $_POST["content"];
                
                $id_room = $_GET["id"];
                
                $date = date("Y-m-d");
                $status = 1;
                $m_comments->add_comments($id_cmt,$id_room,$date,$email,$content,$status);
                
            }
        }
        if(isset($_GET["id"]))
        {
            $id = $_GET["id"];
        }
        $room = $m_room ->read_room_by_idroom($id);
        $comments = $m_comments->read_comments_by_idroom($id);
        $view = 'views/detail_room/v_detail.php';
        include('templates/frontend/layout.php');
    }





}
?>