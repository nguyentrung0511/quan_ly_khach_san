<?php
class c_home{
    public function __construct()
    {
    }

    public function index(){
        include ("models/m_reservation_step.php");
        $m_room = new m_reservation_step();
        $room_category = $m_room->read_room_category();
        $view = 'views/home/v_home.php';
        include ("templates/frontend/layout.php");
    }
}
?>
