<?php
class c_reservation_step1{
    public function __construct()
    {
    }

    public function show()
    {
        $isPaging = true;
        include ("models/m_room.php");
        include ("models/m_category.php");
        include ("models/m_reservation_step.php");

        $m_room = new m_room();
        $room = $m_room->read_room();
        $id_room_category = $room[0]->id_room_category;
        if(isset($_GET['id_room_category']))
        {
            $id_room_category = $_GET['id_room_category'];
        }
        $room = $m_room->read_room_by_category($id_room_category);
        $count=count($room);
        // Phân trang 1
        include("libs/Pager.php");
        $p=new pager();
        $limit=6;
        $count=count($room);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $room=$m_room->read_room_by_category($id_room_category,$vt,$limit);
        $room_full = $m_room->read_room_by_category_full($id_room_category);
        if (isset($_GET["btnCheck"]))
        {
            $isPaging = false;
            $date_arrive = date_create($_GET["arrive"]);
            $arrive = date_format($date_arrive, "Y-m-d");
            $date_departure = date_create($_GET["departure"]);
            $departure = date_format($date_departure, "Y-m-d");
            $m_bill = new m_reservation_step();
            $bill = $m_bill->read_bill();
            $array = json_decode(json_encode($bill), true);
//            echo"<pre>";
//            echo print_r($array);
//            die();
            $arr_room=[];
            $array_room_all =[];
            $m_room_all = new m_room();
            $room_all = $m_room_all->read_room();
            $arr_room_all = json_decode(json_encode($room_all), true);
//            echo"<pre>";
//            echo print_r($arr_room_all);
//            die();

            foreach ($arr_room_all as $key=> $value){
                $array_room_all[] = $value['id'];
            }
//            echo"<pre>";
//            echo print_r($array_room_all);
//            die();
            foreach ($array as $key => $value) {
                if (strtotime($arrive) == strtotime($value["arrive"]) ||
                    strtotime($departure) == strtotime($value['departure']) ||
                    strtotime($arrive) == strtotime($value['departure']) ||
                    strtotime($departure) == strtotime($value['arrive']) ||
                    (strtotime($arrive) > strtotime($value['arrive']) && strtotime($arrive) < strtotime($value['departure']))||
                    (strtotime($departure) > strtotime($value['arrive']) && strtotime($departure) < strtotime($value['departure']))||
                    (strtotime($arrive) < strtotime($value['arrive']) && strtotime($departure) > strtotime($value['departure']))
                ) {
                    $arr_room[] = $value['id_room'];
                }
            }
//                echo"<pre>";
//                echo print_r( $arr_room);
//                die();
            // ket qua phong thoa man
            $results = array_diff($array_room_all, $arr_room);
            $newRoom = [];
//                echo"<pre>";
//                echo print_r( $results);
            foreach($arr_room_all as $item) {
                foreach($results as $result) {
                    if ($item['id'] == $result) {
                        $newRoom[] = $item;
                    }
                }
            }

//
//            echo"<pre>";
//            echo print_r($newRoom);
//            die();
//            $newRooms = $m_room_all->doc_du_lieu($newRoom);


        }

        $view = 'views/booking_room/reservation_step1/v_reservation_step1.php';
        include("templates/frontend/layout.php");
    }


}

?>


