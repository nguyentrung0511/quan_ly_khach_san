<?php
include ("models/m_reservation_step.php");
include ("models/m_room.php");
class c_reservation_step2{

    public  function show_room()
    {
        $m_room = new m_room();
        $room = $m_room->read_room();
        $id = $room[0]->id;
        if(isset($_GET["id"]) && isset($_GET["arrive"]) && isset($_GET["departure"]))
        {
            $id = $_GET["id"];
            $den = $_GET['arrive'];
            $di = $_GET['departure'];
        }

        $rooms = $m_room ->read_room_by_idroom($id);
        $m_customer=new m_reservation_step();
        $service = $m_customer->read_service();
         if(isset($_POST['btnBook'])) {
             $full_name=$_POST['full_name'];
             $address =$_POST['address'];
             $email = $_POST["email"];
             $phone_number=$_POST['phone_number'];
             $id_service = $_POST['id_dich_vụ'];
             $status=1;
             $id_customer= $m_customer->add_customer($full_name,$address,$email,$phone_number,$status);
             if($id_customer>0)
             {
//                 $id_customer, $id_room, $arrive, $departure,$total_bill,$payment_methods,$description,$status
                 $id_room=$id;
//                 $date_arrive = date_create($_POST["arrive"]);
//                 $arrive = date_format($date_arrive, "Y-m-d");
//                 $date_departure = date_create($_POST["departure"]);
//                 $departure = date_format($date_departure, "Y-m-d");
                 $show_service = $m_customer->read_id_service($id_service);
                 $price_service = $show_service->price;
                 $arrive = $den;
                 $departure = $di;
                 $datetime1 = strtotime($arrive);
                 $datetime2 = strtotime($departure);

                 $secs = $datetime2 - $datetime1;// == <seconds between the two times>
                 $days = $secs / 86400;
                 $total_bill=$rooms->price*$days+$price_service;
                 $description=$_POST['description'];
                 $status =1;
                 $bill=$m_customer->add_bill($id_customer, $id_room,$id_service, $arrive, $departure,$total_bill,$description,$status);
                 $max_id = $m_room->read_max_id_bill();
                 $max_id_bill = $max_id->id_max;
                 $this->sendMail($max_id_bill,$id_customer,$email);
                 if($bill>0)
                 {
                     echo "<script>alert('Chúc mừng bạn đã đặt phòng thành công!');
                     window.location='bill_success.php'
                     </script>";
                 }
                 else
                 {
                     echo "<script>alert('Đặt phòng không thành công!')</script>";
                 }
             }
         }

        $view = 'views/booking_room/reservation_step2/v_reservation_step2.php';
        include("templates/frontend/layout.php");

    }


     function sendMail($max_id_bill, $id_customer,$email)
    {

        require_once("libs/Helper.php");
        $dat_phong = new m_room();
        $booking = $dat_phong->read_bill_by_id($max_id_bill);
        $customer = $dat_phong->read_customer_by_id($id_customer);


        $status = "Chưa thanh toán";
        $tieu_de = "Thông báo đặt phòng tại Caesar Hotel";
        $xhtml = "<p><strong>Tên phòng:".$booking->room_name." </strong></p>";
        $xhtml .= "<p><strong>Tình trạng thanh toán: ".$status."</strong></p>";
        $xhtml .= "<p><strong>Quý khách đã đặt phòng thành công, vui lòng kiểm tra lại thông tin đặt phòng </strong></p>";
        $xhtml .= "<p><strong>Tên khách hàng: ". $customer->full_name ."</strong></p>";
        $xhtml .= "<p><strong>Số điện thoại liên hệ: ". $customer->phone_number ." </strong></p>";
        $xhtml .= "<p><strong>Giá phòng/ Day: ".number_format($booking->price).'/Day'." </strong></p>";
        $xhtml .= "<p><strong>Ngày đến: ". $booking->arrive." </strong></p>";
        $xhtml .= "<p><strong>Ngày đi: ". $booking->departure ." </strong></p>";
        $xhtml .= "<p><strong>Tổng tiền: ".number_format($booking->total_bill).'/VND'."</strong></p>";
        $xhtml .= "<p><strong>Cảm ơn quý khách đã lựa chon Caesar Hotel là điểm đến cho chuyến đi này. Chúc Quý khách có một trải nghiệm thú vị cùng với Caesar Hotel!  </strong></p>";
        $xhtml .= "<p><strong>Thông tin liên hệ Quý khách có thể gọi đến số điện thoại: 0388414141   </strong></p>";
        $noi_dung_mail = "<b>Từ: </b>ABCD<p/><b>Email:</b>ABCD<p/>Đặt phòng thành công!";
        $kq=Helper::Gui_mail_lien_he($tieu_de,$xhtml,$email);
    }

}
?>
