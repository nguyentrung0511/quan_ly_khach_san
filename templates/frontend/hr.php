<!--[if IE 7]> <body class="ie7 lt-ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 8]> <body class="ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]> <body class="ie9 lt-ie10"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->

<!-- PRELOADER -->
<div id="preloader">
    <span class="preloader-dot"></span>
</div>
<!-- END / PRELOADER -->

<!-- PAGE WRAP -->
<div id="page-wrap">

    <!-- HEADER -->
    <header id="header">

        <!-- HEADER TOP -->
        <div class="header_top">
            <div class="container">
                <div class="header_left float-left">
                    <span><i class="lotus-icon-cloud"></i> 18 °C</span>
                    <span><i class="lotus-icon-location"></i> Đường Trần Hưng Đạo, Cửa Lấp - Xã Dương Tơ Huyện Đảo Phú Quốc - Tỉnh Kiên Giang, Cửa Lấp, Đảo Phú Quốc, Việt Nam, 92500</span>
                    <span><i class="lotus-icon-phone"></i> 0388414141</span>
                </div>

            </div>
        </div>
        <!-- END / HEADER TOP -->

        <!-- HEADER LOGO & MENU -->
        <div class="header_content" id="header_content">

            <div class="container">
                <!-- HEADER LOGO -->
                <div class="header_logo">
                    <a href="index.php"><img src="public/layout/images/logo.png" alt="" ></a>
                </div>
                <div class="header_logo">
                    <a href="index.php"><img src="public/layout/images/text.png" height="50px" width="100px" style="margin-top: 15px" alt="" ></a>
                </div>
                <!-- END / HEADER LOGO -->

                <!-- HEADER MENU -->
                <nav class="header_menu" style="margin-right: 200px">
                    <ul class="menu">
                        <li >
                            <a href="index.php">Home </a>
                        </li>

                                                <li><a href="#">ROOM CATEGORY <span class="fa fa-caret-down"></a>
                                                    <ul class="sub-menu">
                                                        <?php
                                                        foreach ($category as $ct) {
                                                            ?>
                                                            <li><a href="room.php?id=<?php echo $ct->id;?>"><?php echo $ct->name;?></a></li>
                                                            <?php
                                                        }
                                                        ?>
                                                    </ul>
                                                </li>
                        <li>
                            <a href="reservation_step1.php">Booking Room <span class="fa fa-caret-down"></span></a>
                            <ul class="sub-menu">
                                <li><a href="reservation_step1.php"> Step 1: Choose Room</a></li>
                                <li><a href="reservation_step2.php"> Step 2: Booking</a></li>
                            </ul>
                        </li>


                    </ul>
                </nav>
                <!-- END / HEADER MENU -->

                <!-- MENU BAR -->
                <span class="menu-bars">
                        <span></span>
                    </span>
                <!-- END / MENU BAR -->

            </div>
        </div>
        <!-- END / HEADER LOGO & MENU -->

    </header>
    <!-- END / HEADER -->
